---
title: "Regresión Lineal"
author: "Carlos y Jeniffer Martínez"
date: "12/12/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(readxl)
library(dplyr)
library(corrplot)
library(Rcmdr)
```

##### Cargamos las librerias necesarias para la elaboración del modelo
```{r librerias, echo=TRUE}
library(readxl)
library(dplyr)
library(corrplot)
library(Rcmdr)
```

##### Extraemos los datos de excel y los almacenamos en la variable ventas, ademas convertimos nuestros datos a __data.frame__
```{r ventas, echo=TRUE}
ventas<-read_excel("C:/Users/pc/Desktop/Mineria/r_project/TiendaModelos/datos/lineal.xlsx", sheet="lineal")
ventas<-as.data.frame(ventas)
```

##### Eliminamos los datos que son nulos de nuestras ventas
```{r eliminar, echo=TRUE}
datos<-na.omit(ventas)
```

#### __Analisis de nuetros datos__
##### Primero vemos la estructura de los datos de nuestra variable datos con __str()__
```{r str, echo=TRUE}
str(datos)
```

##### Con la función __glimple()__  Mostramos la cantidad de observaciones y varibales que poseen nuestros datos y los datos mas relevantes
```{r glimpse, echo=TRUE}
glimpse(datos)
```

##### Con la función __class()__ podemos ver el tipo de nuestro conjunto de datos, en nuestro caso data.frame
```{r class, echo=TRUE}
class(datos)
```

##### Vemos como se agrupan los datos de las variables monto y precio unitario para ver si hay valores que se salen de los quartiles
```{r agrupan, echo=TRUE}
boxplot(datos$MONTO, horizontal = T)
boxplot(datos$PRECIO_UNITARIO,horizontal = T)
```

##### Vemos el histograma de los precios unitarios y monto
```{r hist, echo=TRUE}
hist(datos$PRECIO_UNITARIO, col = rainbow(6), main = "Histograma de PRECIOS UNITARIOS")
hist(datos$MONTO, col = rainbow(6), main = "Histograma de MONTO")
```

##### Conocemos los quartiles de monto y precio_unitario, mediante la función de __density()__
```{r density, echo=TRUE}
density(datos$MONTO)
density(datos$PRECIO_UNITARIO)
```

##### Creamos una función para eliminar los outliers que existen en nuestros datos
```{r outliers, echo=TRUE}
impute_outliers <- function(x, removeNA = TRUE){
  quantiles <- quantile(x, c(0.05, 0.90), na.rm = removeNA)
  x[x<quantiles[1]] <- mean(x, na.rm = removeNA)
  x[x>quantiles[2]] <- median(x, na.rm = removeNA)
  x
}
```

##### Despues de crear la función para eliminar los outliers, se eliminan los montos que se salen de los quartiles
```{r eli_outliers, echo=TRUE}
datos$MONTO_IMPUT<-impute_outliers(datos$MONTO)
boxplot(datos$MONTO_IMPUT)
```

#### __Correlaciones de los datos__
##### Vemos las correlaciones que existen en nuestros datos con la función __pairs()__
```{r pairs, echo=TRUE}
plot(datos)
pairs(datos, main = "Correlacion de los datos",
      panel=panel.smooth, cex = 2,
      pch = 20, bg="blue",
      cex.labels = 1, font.labels=1)
```

##### Vemos la matriz de correlaciones simples
```{r cordatos, echo=TRUE}
cor(datos)
```

##### Vemos la correlación entre 2 variables (precio unitario y monto)
```{r corvars, echo=TRUE}
cor(datos$PRECIO_UNITARIO, datos$MONTO)
```

##### Vemos la correclación de forma gráfica 
```{r corrplot, echo=TRUE}
coor<-cor(datos)
corrplot(coor,method = "circle")
```

##### Ahora realizamos el modelo lineal simple y multiple para comparar cual es el que nos refleja un resultado más realista 
```{r lm, echo=TRUE}
lm_simple<-lm(data = datos, PRECIO_UNITARIO~MONTO)
lm_multiple<-lm(data = datos, PRECIO_UNITARIO~MONTO+CANTIDAD_VENDIDA)
```

##### Y vemos el resumen del modelo
```{r summary, echo=TRUE}
summary(lm_simple)
summary(lm_multiple)
```

#### __Gráficamos los datos del modelo__
##### Primero comenzando con el método __head()__ para conocer los datos de nuestro modelo lineal
```{r head, echo=TRUE}
head(lm_simple)
```

##### Después graficamos los datos de nuestras ventas y pintamos la predicción del modelo
```{r plot, echo=TRUE}
plot (datos$PRECIO_UNITARIO,datos$MONTO,xlab = "PRECIO UNITARIO",ylab = "MONTOS VENDIDOS",
      main="datos del modelo lineal") 
abline(lm_simple,col="red")
```

##### Con el metodo __summary()__ podemos ver la significancia y el porcentaje de acierto, los * simbolizan la significancia de las variables y el porcentaje de acierto se encuentra en Multiple R-squared
```{r summaryLM, echo=TRUE}
summary(lm_simple)
```

#### __Predicción__
##### Ahora vamos a hacer predicciones con los 2 modelos, con el modelo multiple vamos a predecir el precio unitario de un producto cuando las ventas son de 250000, y con el modelo multiple tambien vamos a predecir el precio unitario de un producto pero esta vez con unas ventas de 250000 y una cantidad comprada de 1
```{r predictSM, echo=TRUE}
predict(lm_simple,data.frame(MONTO=250000))
predict(lm_multiple,data.frame(MONTO=250000, CANTIDAD_VENDIDA=1))
```

##### Valoramos si es nesesario eliminar variables del modelo, el AIC debe ser el menor , si elimino variables el AIC debe ser menor para poder considerar eliminar.
```{r step, echo=TRUE}
step(lm_simple)
step(lm_multiple)
```

##### Obtenemos el limite inferior y superior del modelo 
```{r limite, echo=TRUE}
predict(lm_multiple,data.frame(MONTO=250000, CANTIDAD_VENDIDA=1),level = 0.95,interval = "prediction")
```
 
##### Creamos una función para agregar el mensaje al resultado final
```{r fmensaje, echo=TRUE}
fmensaje <- function(valor){
  sapply(valor, function(x) if(x < 0) paste(x, "por debajo de valor predicho") else 
    paste (x, "por encima del valor predicho"))
}
```

##### Finalmente creamos las variables de resultados donde almacenaremos un __data.frame__ en el cual creamos los datos predichos para los 2 modelos
```{r resultado, echo=TRUE}
resultado_multiple<-data.frame(datos$MONTO,datos$PRECIO_UNITARIO,lm_multiple$fitted.values,
                      round(datos$PRECIO_UNITARIO-lm_multiple$fitted.values),
                      fmensaje(round(datos$PRECIO_UNITARIO-lm_multiple$fitted.values))
)
resultado_simple<-data.frame(datos$MONTO,datos$PRECIO_UNITARIO,lm_simple$fitted.values,
                      round(datos$PRECIO_UNITARIO-lm_simple$fitted.values),
                      fmensaje(round(datos$PRECIO_UNITARIO-lm_simple$fitted.values))
)
```

##### Renombramos las columnas de nuestros data.frame para que sea más fácil de analizar los datos
```{r resultadonames, echo=TRUE}
names(resultado_multiple)<-c("MONTO","PRECIO_UNITARIO","PRECIO_MODELO","DIFERENCIA","DESCRIPCION")
names(resultado_simple)<-c("MONTO","PRECIO_UNITARIO","PRECIO_MODELO","DIFERENCIA","DESCRIPCION")
```

##### Mostramos los resultados del modelo lineal en el cual podemos observar que los datos no son tan realistas ya que algunos son muy elevados
```{r viewEchoS, echo=TRUE}
head(resultado_simple)
```

##### Mostramos los resultados del modelo multiple, donde podemos ver que en este modelo los datos si son mas realistas y se ajustan a la realidad de las ventas
```{r viewEchoM, echo=TRUE}
head(resultado_multiple)
```
