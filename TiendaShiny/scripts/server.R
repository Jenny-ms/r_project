## server.R ##

server<-function(input, output, session) {
  
  #Grafico baras #1 ventas por proveedores entre fechas y provincias
  output$gventas_proveedor<-renderPlot({
    v_ventas_fechas<-venta_fechas_proveedor %>%
      filter(FECHA>=input$dateRange4[1] & FECHA<=input$dateRange4[2] &
               PROVINCIA_PROVEEDOR %in% input$d4provincias)
    ggplot(data=v_ventas_fechas, aes(x=reorder(PROVEEDOR,-TOTAL,sum), y=TOTAL, fill=PROVEEDOR)) +
      geom_bar(stat="identity") +
      scale_fill_manual(values = disorder_palette_colors()) +
      xlab("PROVEEDOR") +
      stat_summary(
        aes(label = colones(stat(y))), fun.y = 'sum', geom = 'text', col = 'black', vjust = 1.5,
        angle = 20, size = 5
      ) + scale_y_continuous(labels = scales::dollar_format(prefix = "₡")) +
      theme (axis.text.x = element_text(face="bold", colour="black", size=12, angle = 20),
             axis.text.y = element_text(face="plain", colour="black", size=12, hjust=0.5))
  })
  
  #Grafico 2 lineal # total de ventas por vendedores entre mes y cargos
  output$gventas_vendedor<-renderPlot({
    v_ventas_mensuales_vendedor<-venta_anuales_vendedor %>%
      filter(ANNIO %in% input$d4annos & MES %in% input$d4meses & CARGO_VENDEDOR %in% input$d4cargos)
    ggplot(data=v_ventas_mensuales_vendedor, aes(x=MES, y=MONTO, group=paste(VENDEDOR, " (", CARGO_VENDEDOR, ")"),
                                                 colour=paste(VENDEDOR, " (", CARGO_VENDEDOR, ")"))) +
      geom_line() +
      scale_colour_brewer("VENDEDOR", palette="Dark2") +
      xlab("VENDEDOR") + ylab ("MONTO VENDIDO") +
      stat_summary(
        aes(label = colonesVen(stat(y))), fun.y = 'sum', geom = 'text', col = '#212F3C', vjust = 1.5
      ) + scale_y_continuous(labels = scales::dollar_format(prefix = "₡")) +
      geom_point( size=2, shape=21, fill="white") +
      theme (axis.text.x = element_text(face="plain", colour="black", size=12, angle = 10),
             axis.text.y = element_text(face="bold", colour="black", size=12, hjust=0.5))
  })
  
  output$gventas_anuales<-renderPlot({
    v_ventas_anuales<-venta_anuales %>%
      filter(PROVEEDOR %in% input$d5proveedores) 
    ggplot(data = suma_ventas_anuales(v_ventas_anuales), 
           aes(x=ANNIO, y=TOTAL, group="ANNIO", colour=ANNIO)) +
      scale_colour_manual("AÑO", values=c("#8A2BE2", "#A52A2A", "#006400", "#FFD700")) +
      geom_line(stat="identity") + xlab("AÑO") +
      stat_summary(
        aes(label = colones(stat(y))), fun.y = 'sum', geom = 'text', col = '#212F3C', vjust = 1.5, size=5
      ) + scale_y_continuous(labels = scales::dollar_format(prefix = "₡")) +
      geom_point( size=4, shape=21, fill=c("#8A2BE2", "#A52A2A", "#006400", "#FFD700")) +
      theme (axis.text.x = element_text(face="plain", colour="black", size=14, angle = 10),
             axis.text.y = element_text(face="bold", colour="black", size=14, hjust=0.5))
  })
  
  #Tabla de productos
  data<-reactive({
    venta_anuales_producto %>% filter(ANNIO %in% input$d3annos & TIPO_PRODUCTO %in% input$d4tipos &
                                        MARCA_PRODUCTO %in% input$d4marcas)
  })
  output$tblventas_producto<-DT::renderDataTable({
    DT::datatable(data(),
                  options = list(pageLength = 20))
  })
  
  observe({
    updateSelectInput(session, "d4marcas",
                      label = "Marca de producto",
                      choices = marcas[marcas$TIPO_PRODUCTO == input$d4tipos, 1],
                      selected = head(marcas[marcas$TIPO_PRODUCTO == input$d4tipos, 1], 1)
    )
  })
  
  output$totales_producto<-renderValueBox({
    valueBox(
      paste0(colones(sum(data()$MONTO))),
      "Ventas Totales",
      color = "navy"
    )
  })
  
  output$cantidad_productos<-renderValueBox({
    valueBox(
      paste0(nrow(data())),
      "Cantidad Total de Ventas",
      color = "navy"
    )
  })
  
  output$prom_ventas<-renderValueBox({
    valueBox(
      paste0(colones(promedio_ventas(nrow(data()), sum(data()$MONTO)))),
      "Promedio de ventas",
      color = "navy"
    )
  })
  
  #Grafico bara 5 productos menos y mas vendidos por proveedor, tipo de producto y mes.
  filter_productos<-reactive({
    v_productos<-venta_anuales_producto  %>%
      filter(ANNIO %in% input$d5annos & MES %in% input$d5meses &
               TIPO_PRODUCTO %in% input$d5tipos)
    v_productos
  })
  
  output$gproductos_top5<-renderPlot({
    v_productos<-filter_productos()
    ggplot(data=top_5_productos(v_productos), aes(area=TOTAL, fill=colones(TOTAL), label=PRODUCTO)) +
      geom_treemap() +
      scale_fill_manual("TOTAL", values = disorder_palette_colors()) +
      ggtitle ("TOP 5 DE PRODUCTOS MÁS VENDIDOS") +
      geom_treemap_text(fontface = "plain", colour = "black", place = "centre",
                        grow = TRUE, angle = 15) +
      theme (plot.title = element_text(face = "bold", size = 14, vjust = 2, lineheight = 1.5))
  })
  
  output$gproductos_last5<-renderPlot({
    v_productos<-filter_productos()
    ggplot(data=last_5_productos(v_productos), aes(area=TOTAL, fill=colones(TOTAL), label=PRODUCTO)) +
      geom_treemap()+
      scale_fill_manual("TOTAL", values = disorder_palette_colors()) +
      ggtitle ("TOP 5 DE PRODUCTOS MENOS VENDIDOS") +
      geom_treemap_text(fontface = "plain", colour = "black", place = "centre",
                        grow = TRUE, angle = 15) +
      theme (plot.title = element_text(face = "bold", size = 14, vjust = 2, lineheight = 1.5))
  })
  
  #Grafico de barras y pie, ventas de clientes
  filter_clientes<-reactive({
    v_ventas_fecha_cli<-venta_fechas_clientes %>%
      filter(SEXO_CLIENTE %in% input$d5sexo &
               FECHA>=input$dateRange2[1] & FECHA<=input$dateRange2[2])
    v_ventas_fecha_cli
  })
  
  output$gventas_fecha_cliente<-renderPlot({
    v_ventas_fecha_cli<-filter_clientes()
    ggplot(data = top_10_clientes(v_ventas_fecha_cli), aes(x=2, y=MONTO, fill=CLIENTE)) +
      geom_bar(stat = "identity",
               color="white")+
      geom_text(aes(label=colonesVen(stat(y)), fun.y = 'sum'),
                position=position_stack(vjust=0.5),color="black",size=6, angle=20) +
      coord_polar(theta = "y") +
      scale_fill_manual(values=disorder_palette_colors()) +
      theme_void()+
      xlim(0.5,2.5)
  })
  
  output$gventas_sexo<-renderPlot({
    v_ventas_sexo<-filter_clientes()
    ggplot(data = porcentaje_sexo(v_ventas_sexo), aes(x="", y=CANTIDAD, fill=SEXO_CLIENTE)) +
      geom_bar(stat = "identity",
               color="white")+
      geom_text(aes(label=percent(CANTIDAD)),
                position=position_stack(vjust=0.5),color="black",size=6) +
      coord_polar(theta = "y") +
      scale_fill_manual("SEXO", values=c("#E9967A", "#00CED1")) +
      theme_void()
  })
  
}